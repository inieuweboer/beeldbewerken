"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 3: Gaussian_convolution.py

    Calculates, shows and times the performance of different ways
    of convolving an image with the Gaussian density function.
"""

import numpy as np
import scipy.ndimage as image
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import time


# Returns the one dimensional density of the normal distribution at x
def normal_density1d(x, std, stdsq):
    return 1.0 / (np.sqrt(2 * np.pi)*std) * np.exp(-(x*x) / (2 * stdsq))


# Returns the two dimensional density of the normal distrubution at (x,y).
def normal_density2d(x, y, std, stdsq):
    return 1.0 / (2 * np.pi * stdsq) * np.exp(-(x*x + y*y) / (2 * stdsq))


# Returns a [6std x 1] kernel for use in the split convolution
def Gauss1(std):
    # If the standard deviation is 0, normal_density won't work.
    if std == 0:
        return np.array([1])

    # Take six times the standard deviation as matrix size.
    m_size = int((6 * std)) + 1
    h_m_size = 0.5 * m_size
    # Create a flat matrix
    matrix = np.ones(m_size)
    matrix_sum = 0
    stdsq = std * std

    # Fill the matrix with the right values
    for x in range(m_size):
        value = normal_density1d(x - h_m_size, std, stdsq)
        matrix[x] = value
        matrix_sum = matrix_sum + value

    # Normalize the matrix
    matrix = matrix / matrix_sum
    return matrix


# Returns a [6std x 6std] kernel for use in the direct convolution
def Gauss(std):
    # If the standard deviation is 0, normal_density won't work.
    if std == 0:
        return np.array([1])

    # Take six times the standard deviation as matrix size.
    m_size = int((6 * std)) + 1
    h_m_size = 0.5 * m_size
    matrix = np.ones((m_size, m_size))
    matrix_sum = 0
    stdsq = std * std

    # Fill the array with the right values.
    for x in range(m_size):
        for y in range(m_size):
            value = normal_density2d(x - h_m_size, y - h_m_size, std, stdsq)
            matrix[x][y] = value
            matrix_sum = matrix_sum + value

    # Normalize the matrix
    matrix = matrix / matrix_sum
    return matrix


def do_blur1d(img, std=5):
    W1d = Gauss1(std)
    # Convolve over both columns and rows seperately
    img = image.convolve1d(img, W1d, axis=-1, mode='nearest')
    img = image.convolve1d(img, W1d, axis=0, mode='nearest')
    return img


def do_blur2d(img, std=5):
    W = Gauss(std)
    img = image.convolve(img, W, mode='nearest')
    return img


def show_images(imagepath, std=5):
    img = image.imread(imagepath, flatten='true').astype(int)
    blurred2d = do_blur2d(img, std)
    blurred1d = do_blur1d(img, std)

    # Plot original image and both blurred images
    plt.figure().canvas.set_window_title("2D Gaussian Convolution")
    plt.imshow(blurred2d, cmap=cm.Greys_r, vmin=0, vmax=255)
    plt.figure().canvas.set_window_title("1D Gaussian Convolutions")
    plt.imshow(blurred1d, cmap=cm.Greys_r, vmin=0, vmax=255)
    plt.figure().canvas.set_window_title("Original Image")
    plt.imshow(img, cmap=cm.Greys_r, vmin=0, vmax=255)
    plt.show()

    return 0


# Shows kernel as 3D plot
# Inspired by: surface_3d_demo.py from
# http://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html#surface-plots
def show_kernel(std=5):
    """ Shows the kernel calculated from the Gaussian density as a 3d plot"""
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib.ticker import LinearLocator, FormatStrFormatter

    W1d = Gauss(std)
    matrix_size = W1d.shape[0]
    half_matrix_size = matrix_size / 2

    fig = plt.figure()
    ax = Axes3D(fig)
    x = np.arange(-half_matrix_size, half_matrix_size + 1)
    y = np.arange(-half_matrix_size, half_matrix_size + 1)
    X, Y = np.meshgrid(x, y)
    surf = ax.plot_surface(X, Y, W1d, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0)

    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


# Time both methods for values of s in a given array.
def time_methods(imagepath, stds):
    img = image.imread(imagepath, flatten='true').astype(int)

    times1d = np.ones(np.size(stds))
    times2d = np.ones(np.size(stds))
    counter = 0

    for std in stds:
        # Time both methods
        print "Testing for std = " + str(std) + "..."
        start = time.time()
        blurred2d = do_blur2d(img, std)
        time2d = time.time() - start
        start = time.time()
        blurred1d = do_blur1d(img, std)
        time1d = time.time() - start

        # Add measured times to an array
        times1d[counter], times2d[counter] = time1d, time2d
        counter = counter + 1

    # Plot the graphs using the arrays
    plt.figure().canvas.set_window_title("1D Gaussian Convolution")
    plt.plot(stds, times1d)
    plt.figure().canvas.set_window_title("2D Gaussian Convolution")
    plt.plot(stds, times2d)
    plt.show()

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--img', help='Image to convolve',
                        default="cameraman256.png", type=str)
    parser.add_argument('--std', help='Standard deviation',
                        default=5., type=float)
    parser.add_argument('--sg', help='1:Images, 2:Graphs, 3:Show Kernel',
                        default=0., type=int)
    args = parser.parse_args()

    if args.sg == 3:
        show_kernel(args.std)
    elif args.sg == 2:
        time_methods(args.img, range(1, 15))
    else:
        show_images(args.img, args.std)

"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 3: Gaussian_convolution_derivative.py

    Calculates and shows convolutions with derivatives of
    the Gaussian density function, given an image.
"""

import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from Gaussian_convolution import normal_density1d


##########
# Functions that define the derivatives of first and second order of the
# one dimensional Gauss density.
# The derivatives of the two dimensional Gauss density can be expressed
# in these derivatives and the original function due to separatability
# and interchangeability of the sequence in which we take the derivatives
# For the derivation of these results, see the report.

# Returns the value of the derivative with respect to x of the
# one dimensional density of the normal distribution at x
def normal_density1d_deriv1(x, std, stdsq):
    """Derivative of Gaussian density"""
    return -x / stdsq * normal_density1d(x, std, stdsq)


# Returns the value of the second derivative with respect to x of the
# one dimensional density of the normal distribution at x
def normal_density1d_deriv2(x, std, stdsq, std4th):
    """Second derivative of Gaussian density"""
    return (x*x - stdsq) / std4th * normal_density1d(x, std, stdsq)


##########
# Returns a [6std x 1] kernel for convoluting with a
# derivative of certain order (int)
def Gauss_deriv_kernel(std, order):
    """ Calculate 1D kernel for single dimension convolution
    with a derivative of the Gaussian density"""

    # If the standard deviation is 0, normal_density won't work.
    if std == 0:
        return np.array([1])

    if order < 0 or order > 2:
        raise ValueError("Order of derivative must be in [0,2]")

    # Take six times the standard deviation, rounded up, as matrix size.
    matrix_size = int((6 * std)) + 1
    half_matrix_size = 0.5 * matrix_size

    # Create a flat matrix
    matrix = np.empty(matrix_size)
    matrix_sum = 0

    stdsq = std * std

    # Decide which density function to use
    if order == 0:
        def density_func(x, std_aux, stdsq_aux):
            return normal_density1d(x, std_aux, stdsq_aux)
    elif order == 1:
        def density_func(x, std_aux, stdsq_aux):
            return normal_density1d_deriv1(x, std_aux, stdsq_aux)
    elif order == 2:
        std4th = stdsq * stdsq

        def density_func(x, std_aux, stdsq_aux):
            return normal_density1d_deriv2(x, std, stdsq_aux, std4th)

    # Fill the matrix with the right values, depending on
    # the order of the derivative
    for x in xrange(matrix_size):
        value = density_func(x - half_matrix_size, std, stdsq)
        matrix[x] = value
        matrix_sum = matrix_sum + value

    # Normalize the matrix
    matrix = matrix / matrix_sum
    return matrix


##########
# Calculates Gaussian derivative convolution given an image
# img should be a numpy array
# std a float
# xorder and yorder the orders of derivatives, from 0 to 2
def gD(img, std, xorder, yorder):
    """Applies gaussian convolution with derivatives of order given"""

    # Get kernel arrays of certain derivative order
    W1d_x = Gauss_deriv_kernel(std, yorder)
    W1d_y = Gauss_deriv_kernel(std, xorder)

    # Convolve over both columns and rows separately.
    # Axis -1 is horizontally, which convolutes with the y-derivative
    img = ndimage.convolve1d(img, W1d_x, axis=-1, mode='nearest')
    img = ndimage.convolve1d(img, W1d_y, axis=0, mode='nearest')

    return img


def show_derivative_images(imgpath, std=5):
    """Shows the convolutions with the derivative of the Gaussian density
    of all orders up to the second"""

    img = ndimage.imread(imgpath, flatten='true').astype(int)
    blurred_na = gD(img, std, 0, 0)
    blurred_x = gD(img, std, 1, 0)
    blurred_y = gD(img, std, 0, 1)
    blurred_xx = gD(img, std, 2, 0)
    blurred_yy = gD(img, std, 0, 2)
    blurred_xy = gD(img, std, 1, 1)

    # Show original image and all blurred images
    plt.figure().canvas.set_window_title(
            "Gaussian Convolution zero derivative")
    plt.imshow(blurred_na, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Gaussian Convolution derivative to x")
    plt.imshow(blurred_x, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Gaussian Convolution derivative to y")
    plt.imshow(blurred_y, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Gaussian Convolution second derivative to x")
    plt.imshow(blurred_xx, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Gaussian Convolution second derivative to y")
    plt.imshow(blurred_yy, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Gaussian Convolution derivative to xy")
    plt.imshow(blurred_xy, cmap=cm.Greys_r)

    # vmax and vmin set to show the original image
    plt.figure().canvas.set_window_title("Original Image")
    plt.imshow(img, cmap=cm.Greys_r, vmin=0, vmax=255)
    plt.show()


##########
# Perform Gaussian derivative convolution for all partial derivatives
# up to order 2
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
            description="Calculate Gaussian derivative convolution of an "
            "image with standard deviation s and derivative orders i and j")
    parser.add_argument('--imgpath', help='Image to convolve',
                        default="cameraman256.png", type=str)
    parser.add_argument('--std', help='Standard deviation',
                        default=5., type=float)
    args = parser.parse_args()

    show_derivative_images(args.imgpath, args.std)

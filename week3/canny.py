"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 3: canny.py

    Calculates and shows an approximation of the edges using a Canny edge
    detector scheme
"""

import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from Gaussian_convolution_derivative import gD


# Check the 8 pixels around the one given by x,y to check whether
# there is an edge there, with a certain positive tolerance tol
def check_zero_crossing(img_deriv_xx, img_deriv_yy, x, y, tol=30):
    """Checks whether (x,y) is a zero crossing using the second derivatives"""
    retval = False

    # In the x direction, check the pixel left and right
    if((img_deriv_xx[y, x-1] < -tol and img_deriv_xx[y, x+1] > tol) or
       (img_deriv_xx[y, x-1] > tol and img_deriv_xx[y, x+1] < -tol)):
        retval = True

    # In the y direction, check the pixel above and below
    elif((img_deriv_yy[y-1, x] < -tol and img_deriv_yy[y+1, x] > tol) or
         (img_deriv_yy[y-1, x] > tol and img_deriv_yy[y+1, x] < -tol)):
        retval = True

    return retval


# Check the 8 pixels around the one given by x,y to check whether
# there is an edge there, with a certain positive tolerance tol
def check_zero_crossing2(img_deriv_xx, img_deriv_yy, x, y, tol=150):
    """Checks whether (x,y) is a zero crossing using the second derivatives"""
    retval = False

    leftbig = False
    rightbig = False
    leftsmall = False
    rightsmall = False

    topbig = False
    bottombig = False
    topsmall = False
    bottomsmall = False

    # x-direction
    if((img_deriv_xx[y, x-1] < -tol) or
       (img_deriv_xx[y-1, x-1] < -tol) or
       (img_deriv_xx[y+1, x-1] < -tol)):
        leftsmall = True
    if(img_deriv_xx[y, x-1] > tol or
       img_deriv_xx[y-1, x-1] > tol or
       img_deriv_xx[y+1, x-1] > tol):
        leftbig = True
    if((img_deriv_xx[y, x+1] < -tol) or
       (img_deriv_xx[y-1, x+1] < -tol) or
       (img_deriv_xx[y+1, x+1] < -tol)):
        rightsmall = True
    if((img_deriv_xx[y, x+1] > tol) or
       (img_deriv_xx[y+1, x+1] > tol) or
       (img_deriv_xx[y-1, x+1] > tol)):
        rightbig = True
    if(leftbig and rightsmall) or (leftsmall and rightbig):
        retval = True

    # y-direction
    if((img_deriv_yy[y-1, x] < -tol) or
       (img_deriv_yy[y-1, x+1] < -tol) or
       (img_deriv_yy[y-1, x-1] < -tol)):
        topsmall = True
    if(img_deriv_yy[y-1, x] > tol or
       img_deriv_yy[y-1, x+1] > tol or
       img_deriv_yy[y-1, x-1] > tol):
        topbig = True
    if((img_deriv_yy[y+1, x] < -tol) or
       (img_deriv_yy[y+1, x+1] < -tol) or
       (img_deriv_yy[y+1, x-1] < -tol)):
        bottomsmall = True
    if((img_deriv_yy[y+1, x] > tol) or
       (img_deriv_yy[y+1, x+1] > tol) or
       (img_deriv_yy[y+1, x-1] > tol)):
        bottombig = True
    if(topbig and bottomsmall) or (topsmall and bottombig):
        retval = True

    return retval


def gradient_norm(img_deriv_x, img_deriv_y, x, y):
    """Returns the norm of the gradient vector in (x,y)"""
    dx = img_deriv_x[y, x]
    dy = img_deriv_y[y, x]

    return np.sqrt(dx * dx + dy * dy)


def canny(img, std):
    """Calculates and returns the canny plot of an image for a certain
    standard deviation"""

    # We can approximate derivatives using the convolution with
    # derivatives of the Gaussian density
    img_deriv_x = gD(img, std, 1, 0)
    img_deriv_y = gD(img, std, 0, 1)
    img_deriv_xx = gD(img, std, 2, 0)
    img_deriv_yy = gD(img, std, 0, 2)

    img_height, img_width = img.shape

    img_edges = np.zeros((img_height, img_width))

    # Neglects the edges, as defining a zero crossing on an edge
    # is rather arbitrary
    for i in xrange(1, img_height-1):
        for j in xrange(1, img_width-1):
            if check_zero_crossing2(img_deriv_xx, img_deriv_yy, j, i):
                img_edges[i, j] = gradient_norm(img_deriv_x, img_deriv_y, j, i)

    return img_edges


# imgpath should be the relative path to an image
# std is the standard deviation
def show_canny_plot(imgpath, std=5):
    """Shows the canny plot for a certain image given a standard deviation"""
    img = ndimage.imread(imgpath, flatten='true').astype(int)

    img_edges = canny(img, std)

    plt.figure().canvas.set_window_title("Original image")
    plt.imshow(img, cmap=cm.Greys_r, vmin=0, vmax=255)

    plt.figure().canvas.set_window_title("Canny edge plot")
    plt.imshow(img_edges, cmap=cm.Greys_r, vmin=0, vmax=255)
    plt.show()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
            description="Apply canny edge detection and show result")
    parser.add_argument('--imgpath', help='Image to detect edges of',
                        default="cameraman256.png", type=str)
    parser.add_argument('--std', help='Standard deviation',
                        default=5., type=float)
    args = parser.parse_args()

    show_canny_plot(args.imgpath, args.std)

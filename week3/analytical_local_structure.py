"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 3: analytical_local_structure.py

    Code for the visualization of the function
    f(x,y) = Asin(Vx) + Bcos(Wy)
    and its quiver plot

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm


def disp_quiver_plot(A, B, V, W):
    # Define a mesh grid for the discrete approximation F of f
    x = np.arange(-100, 101)
    y = np.arange(-100, 101)
    (Y, X) = np.meshgrid(y, x)

    # Define the function F for the given parameters
    F = A*np.sin(V*X) + B*np.cos(W*Y)

    # Define a mesh grid for the discrete approximation of the first
    # order derivatives
    xx = np.arange(-100, 101, 10)
    yy = np.arange(-100, 101, 10)
    (YY, XX) = np.meshgrid(yy, xx)

    # Define the discrete approximations of the first order
    # derivatives of f for the given parameters
    Fx = A*V * np.cos(V*XX)
    Fy = -B*W * np.sin(W*YY)

    # Show the contour plot of f
    plt.figure().canvas.set_window_title("Contour plot of f")
    plt.imshow(F, cmap=cm.Greys_r, extent=(-100, 100, -100, 100))

    # Show the contour plots of the derivatives
    plt.figure().canvas.set_window_title("Derivative towards x")
    plt.imshow(Fx, cmap=cm.Greys_r, extent=(-100, 100, -100, 100))

    plt.figure().canvas.set_window_title("Derivative towards y")
    plt.imshow(Fy, cmap=cm.Greys_r, extent=(-100, 100, -100, 100))

    # Show the quiver plot overlayed on the greyscale contour plot
    plt.figure().canvas.set_window_title("Quiver plot with gradient vectors")
    plt.imshow(F, cmap=cm.Greys_r, extent=(-100, 100, -100, 100))
    plt.quiver(yy, xx, Fy, -Fx, color='red')
    plt.show()


##########
# Run the function that displays the quiver plot with the given parameters
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
            description="Display a quiver plot for "
            "f(x,y) = Asin(Vx) + Bcos(Wy)")
    parser.add_argument('--A', help='Parameter A', default=1, type=float)
    parser.add_argument('--B', help='Parameter B', default=2, type=float)
    parser.add_argument('--V', help='Parameter V',
                        default=6*np.pi/201, type=float)
    parser.add_argument('--W', help='Parameter W',
                        default=4*np.pi/201, type=float)
    args = parser.parse_args()

    disp_quiver_plot(args.A, args.B, args.V, args.W)

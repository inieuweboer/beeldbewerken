"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 3: analytical_deriv_comparison.py

    Code for the comparison of the analytical vs the compututational
    derivative using convolution with Gaussian density derivatives

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from Gaussian_convolution_derivative import gD


# Compare analytical derivatives and Gaussian smoothing/convolution
def gauss_ratio(F, F_deriv, xorder=1, yorder=0):
    F_convolved = gD(F, 1, xorder, yorder)
    F_ratio = F_deriv / F_convolved

    # Show the derivative of the function, the convolution with
    # the derivative of certain order and the ratio between the two
    plt.figure().canvas.set_window_title(
            "Function derivative, calculated analytically")
    plt.imshow(F_deriv, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Gaussian convolution derivative")
    plt.imshow(F_convolved, cmap=cm.Greys_r)

    plt.figure().canvas.set_window_title(
            "Ratio between analytical and convoluted result")
    plt.imshow(F_ratio, cmap=cm.Greys_r)
    plt.show()


if __name__ == "__main__":
    # Define a mesh grid and the discrete approximations of F and Fx
    x = np.arange(-100, 101)
    y = np.arange(-100, 101)
    (Y, X) = np.meshgrid(y, x)

    A = 1
    B = 2
    V = 6*np.pi/201
    W = 4*np.pi/201

    F = A*np.sin(V*X) + B*np.cos(W*Y)
    Fx = A*V * np.cos(V*X)

    gauss_ratio(F, Fx)

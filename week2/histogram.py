"""
  Ismani Nieuweboer - 10502815
  Lucas Slot        - 10610952
  DB Wiskunde en Informatica

  histogram.py

  Implementation of discrete histogram equalization
"""


from numpy import *
import scipy.ndimage as image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import copy

def histogramEqualization (image, vRange = 255, bins = 100):
	histogram = makeHistogram(image, vRange = vRange, bins = bins)
	cmHistogram = cumsum(histogram)

	# Get dimensions, copy the image.
	pixels = size(image)
	columns = size(image[0])
	rows = pixels / columns
	imageCopy = copy.deepcopy(image)

	##  Construct a lookup table  ##
	# We want a point for each value.
	x = range(vRange + 1)
	# We have points at 0, 1 * (range/bins), 2 * (range/bins), .., range.
	xvals = np.interp(range(bins), [0, bins - 1], [0, vRange])
	# And the values for these points are stored in the cumulative histogram.
	yvals = copy.copy(cmHistogram).astype(float)

	# Now create *range* values out of our original *bins* values. We immediately normalize 
	# this lookup table (by dividing by *pixels*) and then multiply by *vRange* to get the
	# correct gray-scale values.
	LUTable = np.interp(x, xvals, yvals) * vRange / pixels
	
	# Now convert the gray-scale values using the lookup table.
	for row in range(rows):
		for col in range(columns):
			# + 0.5 as a trick for rounding.
			imageCopy[row][col] = int(LUTable[imageCopy[row][col]] + 0.5)
			
	return imageCopy, histogram, makeHistogram(imageCopy, bins = bins)	
	
# Given a 2D-array of values 0-N and a constant b < N, the number of bins 
# this function returns a histogram with binsize N / b (rounded to fit) of 
# this array. This histogram is not normalized.
def makeHistogram (array, vRange = 255, bins = 100):
	histogram = [0] * bins

	# Loop through image and count.
	for row in array:
		for value in row:
			# Find the correct bin (multiply before dividing) and increment it.
			bin = (value * bins) / (vRange + 1)
			histogram[bin] = histogram[bin] + 1
	return histogram

def main (imagepath, bins):
	# Import the image: we can simply flatten the image here as we only import
	# greyscale images (that are encoded as RGB images).
	img = image.imread(imagepath, flatten = 'true').astype(int)

	newImg, oldHist, newHist = histogramEqualization(img, bins=bins)

	# Print histogram, cumulative histogram and image (both old and new).
	plt.bar(range(bins), oldHist)
	plt.figure()
	plt.bar(range(bins), newHist)
	plt.figure()
	plt.bar(range(bins), cumsum(oldHist))
	plt.figure()
	plt.bar(range(bins), cumsum(newHist))
	plt.figure()
	# Set vmin and vmax to prevent matplotlib from performing equalization
	plt.imshow(img, cmap = cm.Greys_r, vmin = 0, vmax = 255)
	plt.figure()
	# Set vmin and vmax to prevent matplotlib from performing equalization
	plt.imshow(newImg, cmap = cm.Greys_r, vmin = 0, vmax = 255)

	plt.show()

if __name__ == "__main__":
    main("Test2.png", 100)

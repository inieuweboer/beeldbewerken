"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 2: transforms.py

    Contains the transformation functions for exercise 1.

"""

import numpy as np
import cv2

##########
# image has to be of type np.ndarray
# (x_i,y_i) for i =1,2,3 have to be integer valued colinear points of the image
# MxN is the dimension of the new image
def affineTransform (image, x1, y1, x2, y2, x3, y3, M, N):
    """Performs an affine transformation on a given image, given three points
    on it and the dimension of the new image
    It is assumed that (x1,y1) -> (0,0), (x2,y2) -> (M,0), (x3,y3) -> (0,N)
    If the three points are colinear, an exception is thrown
    """

    if not isinstance(image, np.ndarray):
        raise TypeError("Numpy array must be provided as image")
    if not all([isinstance(x, (int,long)) for
                x in [x1, y1, x2, y2, x3, y3, M, N]]):
        raise TypeError("Integers must be provided for "
                        "the points and dimension")

    # Checks whether three points are colinear
    if points_colinear(x1, y1, x2, y2, x3, y3):
        raise ValueError("points are colinear")

    # Now define the 6x6 relation matrix
    relation_mat = np.array([[x1, y1, 1, 0, 0, 0],
                             [0, 0, 0, x1, y1, 1],
                             [x2, y2, 1, 0, 0, 0],
                             [0, 0, 0, x2, y2, 1],
                             [x3, y3, 1, 0, 0, 0],
                             [0, 0, 0, x3, y3, 1]])

    # Calculate the coefficients a..f
    # It is assumed that (x1,y1) -> (0,0), (x2,y2) -> (M,0), (x3,y3) -> (0,N)
    # point_vect represents [x1', y1', x2', y2', x3', y3']
    point_vect = np.array([0, 0, M-1, 0, 0, N-1])
    coef_vect = np.linalg.solve(relation_mat, point_vect)

    # Reshape the coefficient vector into a 2x3 matrix.
    affine_mat = coef_vect.reshape(2,3)

    # Applies the transformation, interpolates on the original image
    # and assigns value to new matrix for each pixel

    # cv2 uses columns,rows order, and needs type conversions
    img_new = cv2.warpAffine(image.astype('uint8'),
                             np.float32(affine_mat), (N,M))

    return img_new


##########
def perspectiveTransform (image, x1, y1, x2, y2, x3, y3, x4, y4, M, N):
    """Performs a perspective transformation on a given image,
    given four points on it and the dimension of the new image
    It is assumed that (x1,y1) -> (0,0), (x2,y2) -> (M,0),
    (x3,y3) -> (0,N), (x4,y4) -> (M,N)
    If any three points are colinear, an exception is thrown
    """

    if not isinstance(image, np.ndarray):
        raise TypeError("Numpy array must be provided as image")
    if not all([isinstance(x, (int,long)) for
                x in [x1, y1, x2, y2, x3, y3, x4, y4, M, N]]):
        raise TypeError("Integers must be provided for "
                        "the points and dimension")

    # Checks whether three points are colinear
    if points_colinear(x1, y1, x2, y2, x3, y3) or \
       points_colinear(x1, y1, x2, y2, x4, y4) or \
       points_colinear(x1, y1, x3, y3, x4, y4) or \
       points_colinear(x2, y2, x3, y3, x4, y4):
        raise ValueError("Three points are colinear")

    # Now define the 8x9 relation matrix
    # It is assumed that (x1,y1) -> (0,0), (x2,y2) -> (M,0),
    #(x3,y3) -> (0,N), (x4,y4) -> (M,N)
    relation_mat = np.array([[x1, y1, 1, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, x1, y1, 1, 0, 0, 0],
                             [x2, y2, 1, 0, 0, 0, -M*x2, -M*y2, -M],
                             [0, 0, 0, x2, y2, 1, 0, 0, 0],
                             [x3, y3, 1, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, x3, y3, 1, -N*x3, -N*y3, -N],
                             [x4, y4, 1, 0, 0, 0, -M*x4, -M*y4, -M],
                             [0, 0, 0, x4, y4, 1, -N*x4, -N*y4, -N]])

    # Calculate the coefficients a..i; we can do this using the SVD:
    # rel_mat = U * S * V^T; and taking the last column of the matrix V
    # Since the third output of linalg.svd returns the transpose of V
    # from the SVD, we need the last row of v = V^T

    # This is V^T
    v = np.linalg.svd(relation_mat)[2]

    # Take the last row; this is always the ninth, since the relation
    # matrix is 8x9, V has dimension 9x9
    coef_vect = v[8]

    # Reshape the coefficient vector into a 3x3 coefficient matrix.
    perspec_mat = coef_vect.reshape(3,3)

    # Applies the transformation, interpolates on the original image
    # and assigns value to new matrix for each pixel

    # cv2 uses columns,rows order, and needs type conversions
    img_new = cv2.warpPerspective(image.astype('uint8'),
                             np.float32(perspec_mat), (N,M))

    return img_new


##########
# Three points are colinear iff two slopes calculated from two (different)
# pairs of points are equal iff
# (y_2 - y_1) / (x_2 - x_1)== (y_3 - y_1) / (x_3 - x_1)
# the return value has this eq rewritten to avoid divisions
def points_colinear (x1, y1, x2, y2, x3, y3):
    """Returns boolean value whether the three given points
    on the plane are colinear"""
    return (y2 - y1) * (x3 - x1) == (y3 - y1) * (x2 - x1)


##########
# Some tests on the transformation functions.
if __name__ == "__main__":
    import scipy.misc as misc
    import matplotlib.pyplot as plt

    lena = misc.lena()

    lena_affine = affineTransform(
            lena, 300,400, 200,250, 400,350, 100,100)
    plt.imshow(lena_affine)
    plt.show()

    lena_perspective = perspectiveTransform(
            lena, 200,200, 300,180, 180,300, 400,400, 100,100)
    plt.imshow(lena_perspective)
    plt.show()

    cube = misc.imread('images/sumcube5.png')
    cube_affine = affineTransform(
            cube, 300,300, 150,80, 80,150, 300,300)
    plt.imshow(cube_affine)
    plt.show()

    cube_perspective = perspectiveTransform(
            cube, 50,50, 150,30, 30,150, 170,170, 80,80)
    plt.imshow(cube_perspective)
    plt.show()

"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 5: persp_transf.py

    Contains the perspective transformation function from
    week 2 exercise 1, expanded to be able to transform an
    image given multiple point correspondences.

"""

import numpy as np


##########
# pt_corresp1 and pt_corresp2 should be np arrays with
# pt_corresp1[i] == [x1, y1] corresponding to pt_corresp2 == [x2, y2]
# It is assumed that no three point correspondences are colinear.
# Checking this automatically would require n choose 3 function calls!
def get_persp_transf(pt_corresp1, pt_corresp2):
    """Returns perspective transform given two arrays of point correspondences
    """

    n = pt_corresp1.shape[0]

    if n != pt_corresp2.shape[0]:
        raise ValueError("Amount of correspondences do not match")
    if pt_corresp1.shape[1] != 2 or pt_corresp2.shape[1] != 2:
        raise ValueError("Matrix does not consist of stacked points")

    # Now define the 2nx9 relation matrix
    relation_mat = np.zeros((2*n, 9))

    for i in xrange(n):
        # (x1,y1) corresponding to (x2,y2)
        (x1, y1) = pt_corresp1[i]
        (x2, y2) = pt_corresp2[i]

        # Assign following values to matrix block number i:
        # [x1, y1, 1, 0, 0, 0, -x1*x2, -y1*x2, -x2],
        # [0, 0, 0, x1, y1, 1, -x1*y2, -y1*y2, -y2]])
        relation_mat[2*i, 0] = relation_mat[2*i + 1, 3] = x1
        relation_mat[2*i, 1] = relation_mat[2*i + 1, 4] = y1
        relation_mat[2*i, 2] = relation_mat[2*i + 1, 5] = 1

        relation_mat[2*i, -3] = -x1 * x2
        relation_mat[2*i, -2] = -y1 * x2
        relation_mat[2*i, -1] = -x2

        relation_mat[2*i + 1, -3] = -x1 * y2
        relation_mat[2*i + 1, -2] = -y1 * y2
        relation_mat[2*i + 1, -1] = -y2

    # Calculate the coefficients a..i; we can do this using the SVD:
    # rel_mat = U * S * V^T; and taking the last column of the matrix V
    # Since the third output of linalg.svd returns the transpose of V
    # from the SVD, we need the last row of v = V^T

    # This is V^T
    v = np.linalg.svd(relation_mat)[2]

    # Take the last row; this is always the ninth, since the relation
    # matrix is 2nx9, V has dimension 9x9
    coef_vect = v[-1]

    # Reshape the coefficient vector into a 3x3 coefficient matrix.
    perspec_mat = coef_vect.reshape(3, 3)

    return perspec_mat


##########
# Three points are colinear iff two slopes calculated from two (different)
# pairs of points are equal iff
# (y_2 - y_1) / (x_2 - x_1)== (y_3 - y_1) / (x_3 - x_1)
# the return value has this eq rewritten to avoid divisions
def points_colinear(x1, y1, x2, y2, x3, y3):
    """Returns boolean value whether the three given points
    on the plane are colinear"""
    return (y2 - y1) * (x3 - x1) == (y3 - y1) * (x2 - x1)


##########
# Some tests on the transformation functions.
if __name__ == "__main__":
    import scipy.misc as misc

    lena = misc.lena()

#!/bin/sh
name="nieuweboer_ismani__slot_lucas_CP4_SIFT"
tarname=$name".tar.gz"

mkdir $name -v
cp persp_transf.py ransac.py sift.py README $name/ -R -v
cp images results docs $name/ -R -v

tar -czvf $tarname $name/
rm $name/ -R -v

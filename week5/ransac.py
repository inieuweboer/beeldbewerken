"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 5: ransac.py

    Code for finding the perspective matrix
    given the sets of matching m using
    RANSAC.

"""

import numpy as np
from random import sample
import cv2

import sift
from persp_transf import get_persp_transf


##########
# match_points are the matched points detected by SIFT from the lab code
# tolsq is the quadratic tolerance, as we compare to the Euclidean distance
# without sqrt
# min_inl is the minimum amount of inliers, i.e. the minimum amount of
# newly matching points that fit the perspective transform by the tolerance
def test_hypo_matrix(match_points, tolsq, min_inl):

    amt_matches = len(match_points)
    try:
        indices = sample(xrange(amt_matches), 4)
    except ValueError:
        print "Not enough match points.\n"
        raise

    # Select the keypoints
    keypt_img1 = np.array(
            [match_points[i][0].pt for i in xrange(amt_matches)])
    keypt_img2 = np.array(
            [match_points[i][1].pt for i in xrange(amt_matches)])
    pt_corresp1 = keypt_img1[indices, :]
    pt_corresp2 = keypt_img2[indices, :]

    persp_mat = get_persp_transf(pt_corresp1, pt_corresp2)

    # Create a matrix with the columns consisting of the homogeneous
    # coordinates from the originals taken from the keypoints
    keypt_img1_hom = np.append(keypt_img1.T,
                               np.ones((1, amt_matches)),
                               axis=0)

    # This matrix consists of vectors (sx', sy', s). Divide by the last
    # row to correct for the scale. Then the last row will consists of
    # 1's, so we don't need it
    model_points_uncor = np.dot(persp_mat, keypt_img1_hom)
    model_points = (model_points_uncor / model_points_uncor[-1])[0:-1]

    # Calculate matrix containing Euclidean distances between model points
    # and the corresponding matching points
    # without sqrt, as minimizing sqrt is equivalent
    # to minimizing witout (sqrt is a monotone function)
    diff_mat = model_points.T - keypt_img2

    # Pythagoras on difference of x and y:
    # Add squared entries to form an n x 1 matrix
    eucl_dist_mat = diff_mat[:, 0]**2 + diff_mat[:, 1]**2

    # Find all matchpoints within tolerance and count them
    boolmat = (eucl_dist_mat < tolsq)
    inl_amt = np.sum(boolmat)

    # Only calculate a new matrix and find the error if we have
    # enough inliers.
    if inl_amt >= min_inl:
        # Index the first point set with the boolean matrix, and
        # calculate another perspective matrix
        new_pt_corresp1 = keypt_img1[boolmat]
        new_pt_corresp2 = keypt_img2[boolmat]
        new_mat = get_persp_transf(new_pt_corresp1, new_pt_corresp2)

        return new_mat, inl_amt

    else:
        return None, 0


##########
# match_points : the matching points from SIFT
# tolsq : the RANSAC tolerance squared
# min_inl : The minimum amount of inliers to consider a matrix
# iters : the amount of iterations
def find_matrix(match_points, tolsq=0.01, min_inl=4, iters=500):

    best_amt = min_inl
    count = 0

    while (count < iters):
        count = count + 1
        cur_mat, inl_amt = test_hypo_matrix(match_points, tolsq, best_amt)
        if inl_amt > best_amt:
            best_amt = inl_amt
            best_mat = cur_mat

    return best_mat, best_amt


##########
# img1, img2 two images
def stitch_images(img1, img2, tol, show=True, save=True):
    s = sift.SIFT()

    # Find the keypoints and descriptors for both images
    keys1, des1 = s.detectAndCompute(img1, None)
    keys2, des2 = s.detectAndCompute(img2, None)

    p1, p2, match_points = sift.matchDescriptors(keys1, des1, keys2, des2)
    P, best_amt = find_matrix(match_points, tolsq=tol*tol)

    print "Best amount of inliers: {}".format(best_amt)

    invP = np.linalg.inv(P)

    (img1_y, img1_x, _) = img1.shape
    (img2_y, img2_x, _) = img2.shape

    x_min = 0
    x_max = img1_x
    y_min = 0
    y_max = img1_y

    # Check all four corners of the second image with inverse perspective
    # to find the new image dimensions
    for (y, x) in [(0, 0), (img2_y, 0), (0, img2_x), (img2_y, img2_x)]:

        new_coord_uncor = np.dot(invP, np.array([x, y, 1]))
        x_cand, y_cand = (new_coord_uncor / new_coord_uncor[-1])[0:-1]
        x_min = min(x_min, x_cand)
        y_min = min(y_min, y_cand)
        x_max = max(x_max, x_cand)
        y_max = max(y_max, y_cand)

    # Inverse translate by -x_min, -y_min
    invT = np.array([[1, 0, -x_min], [0, 1, -y_min], [0, 0, 1]])

    # Essentially we want the inverse of translation of image2
    # after perspective transf
    newinvP = np.dot(invT, invP)

    # Now we have the new inverse in FP, round off for indexing
    # add some slack to be able to see the edges
    e = 10
    x_new = int(x_max - x_min) + e
    y_new = int(y_max - y_min) + e
    x_min = int(x_min)
    y_min = int(y_min)

    # Warp from image2 to image1 because image 1 is nicely upright
    # In the warped version of image2 overwrite with data from image 1.
    stitch_img = cv2.warpPerspective(img2, newinvP, (x_new, y_new))

    if show:
        cv2.waitKey(1)
        cv2.imshow('preresult', stitch_img)
        cv2.waitKey(0)

    # The indexing assures that img1 is translated too
    stitch_img[-y_min:img1_y - y_min, -x_min:img1_x - x_min, :] = img1

    if show:
        cv2.waitKey(1)
        cv2.imshow('img1', img1)
        cv2.imshow('img2', img2)
        cv2.imshow('result', stitch_img)
        cv2.waitKey(0)
    if save:
        cv2.imwrite('result.png', stitch_img)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--img1', help='First object image path',
                        default="images/nachtwacht1.jpg", type=str)
    parser.add_argument('--img2', help='Second object image path',
                        default="images/nachtwacht2.jpg", type=str)
    parser.add_argument('--tol', help='Tolerance for RANSAC',
                        default=0.1, type=float)
    parser.add_argument('--show', help='Whether to show result with cv2',
                        default=1)
    parser.add_argument('--save', help='Whether to save result to result.png')
    args = parser.parse_args()

    img1 = cv2.imread(args.img1)
    img2 = cv2.imread(args.img2)

    stitch_images(img1, img2, args.tol, args.show, args.save)

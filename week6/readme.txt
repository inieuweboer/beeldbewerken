### Ismani Nieuweboer - 10502815
### Lucas Slot        - 10610952
### DB Wiskunde en Informatica
###
### Beeldbewerken week 6: readme.txt

Simply running
	python draw.py
will run a demo of the code
which will draw a few cubes of different sizes as well as animate
a cube moving over the circle X^2 + Y^2 = 49.

A number of arguments can be added by running
	python draw.py --arg1 arg1value --arg2 arg2value ...
for a full list of arguments with an explanation of what they do
you can run
	python draw.py --help

If you want to check the cubes/hardcode another one, do so around line 308.

Animations can be saved. We have put an .mp4 file that shows
an animation in the /animations directory.

NOTE 1: be careful when saving animations with a lot of frames, as
this will take quite a while and temporary image will be created
in the running directory for each(!) frame of the animation.
(if you CTRL-C or CTRL-Z the program these won't be deleted)

NOTE 2: On one of our machines the images would be flipped when
loaded with numpy. We do not know exactly what causes this but if
this happens you can use --flip to flip the image.

"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 6: calibration.py

    Contains the function that calibrates the camera
    given the corresponding points. Also contains a
    function that measures the reprojection error of a
    projection matrix given calibration points.

"""

import numpy as np


#########
# Find the calibration matrix P
def calibration_matrix(xy, XYZ):

    # Check if we have equally many calibration points
    if len(xy) == len(XYZ):
        n = len(xy)
    else:
        raise ValueError("Inequal amount of calibration points")

    # Check if the matrix dimensions are correct
    if (xy.shape[1] != 2 or XYZ.shape[1] != 3):
        raise ValueError("Incorrect matrix dimensions")

    # Compose A
    # Start with an empty [n+1 x 12] matrix
    A = np.zeros((2 * n, 12))
    # Enter the nonzero entries of A per two rows.
    for i in xrange(n):
        # q = [X_i Y_i Z_i 1]
        q = np.append(XYZ[i, :], [1])
        # Part 1: [X_i Y_i Z_i 1 0 0 0 0 . . . .]
        A[2 * i, xrange(4)] = q

        # Part 2: [0 0 0 0 X_i Y_i Z_i 1 . . . .]
        A[2 * i + 1, xrange(4, 8)] = q
        # Part 3: [. . . . . . . . * * * *]
        A[2 * i, xrange(8, 12)] = -xy[i, 0] * q
        A[2 * i + 1, xrange(8, 12)] = -xy[i, 1] * q

    # Find P using the SVD trick
    v = np.linalg.svd(A)[2]
    coef_vect = v[-1]
    P = coef_vect.reshape(3, 4)

    return P


##########
# Find the reprojection error
def reproj_error(xy, XYZ, P):

    # Check if we have equally many calibration points
    if(len(xy) == len(XYZ)):
        n = len(xy)
    else:
        raise ValueError("Inequal amount of calibration points")

    # Check if the matrix dimensions are correct
    if (xy.shape[1] != 2 or XYZ.shape[1] != 3 or P.shape != (3, 4)):
        raise ValueError("Incorrect matrix dimensions")

    # Apply the matrix to the 2D calibration points and correct
    reproj_uncor = np.dot(P, np.append(XYZ.T, np.ones((1, n)), axis=0))
    reprojection = (reproj_uncor / reproj_uncor[-1])[0:-1]

    # Find the matrix of difference vectors
    diff_mat = xy.T - reprojection

    # Find the error as the average of the euclidian norm of the difference
    # vectors
    eucl_vect_sq = np.square(diff_mat[0, :]) + np.square(diff_mat[1, :])
    error = np.average(np.sqrt(eucl_vect_sq))

    return error

if __name__ == "__main__":
    # Load the calibration points from text files
    xy = np.loadtxt('data/2D_calibrationpoints')
    XYZ = np.loadtxt('data/3D_calibrationpoints')

    P = calibration_matrix(xy, XYZ)
    error = reproj_error(xy, XYZ, P)

    print "Found the following reprojection matrix: \n"
    print str(P) + "\n"
    print "which has a reprojection error of " + str(error)

# -*- coding: utf-8 -*-
"""
    Ismani Nieuweboer - 10502815
    Lucas Slot        - 10610952
    DB Wiskunde en Informatica

    Beeldbewerken week 6: draw.py

    Contains the function that draws '3D' cubes
    on the image using the projection matrix.

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

from calibration import calibration_matrix
from calibration import reproj_error


#########
# Returns the (X,Y,0) coordinate of a point on the circle given by
# X^2 + Y^2 = rad^2 given a time t and and a total time T.
def circle_param(t, T, rad):
    # 2pi is a full circle
    t = 2.0 * np.pi * float(t)
    return rad * np.cos(t / T), rad * np.sin(t / T), 0


# Returns the (X,Y,0) coordinate of a point on the square with side 2*rad
# given a time t and and a total time T.
def square_param(t, T, rad):
    X = 0
    Y = 0
    Z = 0

    # If t = 0, then X = rad. If t = T/4.0, then X = -rad.
    # This gives exactly a side 2*rad.
    # Note that all parametrizations are linear, so they must be correct.
    if t < (T/4.0):
        X = rad*(1 - 8.0*t/T)
        Y = rad
    # If t = T/4.0, then Y = rad. If t = T/2.0, then Y = -rad.
    elif t < (T/2.0):
        X = -rad
        Y = rad*(3 - 8.0*t/T)
    # If t = T/2.0, then X = -rad. If t = T*3.0/4, then X = rad.
    elif t < (T * 3.0/4):
        X = rad*(-5 + 8.0*t/T)
        Y = -rad
    # If t = T*3.0/4, then Y = -rad. If t = T, then Y = rad.
    elif t < T:
        X = rad
        Y = rad*(-7 + 8.0*t/T)

    return X, Y, Z


#########
# Convert 3D vectors to 2D vectors using a transformationmatrix P.
def transform(P, vectors):
    # Convert homogeneous coords of vectors to 2D using the projection matrix
    # Correct by dividing by the last row, then the last row will
    # consist of ones and is not needed
    # Finally transpose back to use (X,Y) coords again
    n = len(vectors.T)
    vectors_homog = np.append(vectors, np.ones((1, n)), axis=0)
    reproj_uncor = np.dot(P, vectors_homog)
    reproj_vectors = (reproj_uncor / reproj_uncor[-1])[0:-1]
    return reproj_vectors.T


#########
# Returns an array of the corners of a cube transformed to 2D
#
# P : the tranformation matrix
# (X,Y,Z) : the 3D location of the bottom left back corner of the cube
# c : the size of one side of the cube
def cube_corners(P, X, Y, Z, c):
    if c <= 0:
        raise ValueError("Cube size must be positive")

    # First find the 3D coordinates of the corners
    # assuming (X,Y,Z) is the bottomleftback (0) of the cube.
    corners = np.array([[X,     Y,     Z], [X,     Y,     Z + c],
                        [X,     Y + c, Z], [X,     Y + c, Z + c],
                        [X + c, Y,     Z], [X + c, Y,     Z + c],
                        [X + c, Y + c, Z], [X + c, Y + c, Z + c]])

    ############################
    # This corresponds to the  #
    # following cube (by array #
    # indices):                #
    #                          #
    #       1------3           #
    #  X    |`.    |`.         #
    #    ↘  |  `5--+---7       #
    #       |   |  |   |       #
    # Y →   0---+--2   |       #
    #        `. |   `. |       #
    #          `4------6       #
    #                          #
    #       ↑                  #
    #       Z                  #
    ############################

    return transform(P, corners.T)


#########
# Calculates the lines for drawing a perspective cube using
# the matrix P for the transform and returns in list form
def get_cube_lines(P, X, Y, Z, c):

    # Find the converted corners
    s = cube_corners(P, X, Y, Z, c)

    lines = []

    # Create lines using the picture of the cube and add them to
    # the lines list.
    lines.append(plt.Line2D(s[[0, 1], 0], s[[0, 1], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[0, 2], 0], s[[0, 2], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[0, 4], 0], s[[0, 4], 1], ls='-', color='red'))

    lines.append(plt.Line2D(s[[3, 1], 0], s[[3, 1], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[3, 2], 0], s[[3, 2], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[3, 7], 0], s[[3, 7], 1], ls='-', color='red'))

    lines.append(plt.Line2D(s[[5, 1], 0], s[[5, 1], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[5, 4], 0], s[[5, 4], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[5, 7], 0], s[[5, 7], 1], ls='-', color='red'))

    lines.append(plt.Line2D(s[[6, 2], 0], s[[6, 2], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[6, 4], 0], s[[6, 4], 1], ls='-', color='red'))
    lines.append(plt.Line2D(s[[6, 7], 0], s[[6, 7], 1], ls='-', color='red'))

    return lines


#########
# An existing lines list is updated using the same parameters
def update_cube(lines, P, X, Y, Z, c, i):
    if i == 0:  # Do not update in the first frame
        return

    # Find the converted corners
    s = cube_corners(P, X, Y, Z, c)

    # The 0-1-5-4 face disappears into the table parallel to the XZ plane
    # The 0-1-3-2 face appears from the table parallel to the YZ plane
    # The interior of the table is given by X < 0 and Y < 0
    # Note that both on entering and leaving the lines are set to visible,
    # to make sure this works for all definable paths
    # The newcorners are constructed using the picture of the cube

    # Cube is halfway through XZ plane
    # Set all lines to visible if not already the case
    if (Y + c > 0 and X < 0 and Y < 0):
        if not lines[0].get_visible():
            for line in lines:
                line.set_visible(True)
        # We want exactly Y = 0 <=> point is on XZ plane
        newcorners = np.array([[X,     0, Z    ], [X,     0, Z + c],
                               [X + c, 0, Z + c], [X + c, 0, Z    ]])
        newcorners = transform(P, newcorners.T)
        s[[0, 1, 5, 4]] = newcorners
    # Cube is fully contained in object
    # Set all lines to invisible if not already the case
    elif (Y + c < 0 and X + c < 0) and lines[0].get_visible():
        for line in lines:
            line.set_visible(False)
    # Cube is halfway through YZ plane
    # Set lines to visible if not already the case
    elif (X + c > 0 and X < 0 and Y < 0):
        if not lines[0].get_visible():
            for line in lines:
                line.set_visible(True)
        # We want exactly X = 0 <=> point is on YZ plane
        newcorners = np.array([[0, Y,     Z    ], [0, Y,     Z + c],
                               [0, Y + c, Z + c], [0, Y + c, Z    ]])
        newcorners = transform(P, newcorners.T)
        s[[0, 1, 3, 2]] = newcorners

    # Only update lines if visible
    if lines[0].get_visible():
        # Update lines using the picture of the cube
        lines[0] .set_data(s[[0, 1], 0], s[[0, 1], 1])
        lines[1] .set_data(s[[0, 2], 0], s[[0, 2], 1])
        lines[2] .set_data(s[[0, 4], 0], s[[0, 4], 1])

        lines[3] .set_data(s[[3, 1], 0], s[[3, 1], 1])
        lines[4] .set_data(s[[3, 2], 0], s[[3, 2], 1])
        lines[5] .set_data(s[[3, 7], 0], s[[3, 7], 1])

        lines[6] .set_data(s[[5, 1], 0], s[[5, 1], 1])
        lines[7] .set_data(s[[5, 4], 0], s[[5, 4], 1])
        lines[8] .set_data(s[[5, 7], 0], s[[5, 7], 1])

        lines[9] .set_data(s[[6, 2], 0], s[[6, 2], 1])
        lines[10].set_data(s[[6, 4], 0], s[[6, 4], 1])
        lines[11].set_data(s[[6, 7], 0], s[[6, 7], 1])


#########
# Draw a '3D' cube of size c in the image at (X,Y,Z) using get_cube_lines
# P is the perspective matrix
# (X,Y,Z) is the 3D location of the bottom left back corner of the cube
# verbose indicates whether to print some info when a cube is drawn
def draw_cube(ax, P, X=0, Y=0, Z=0, c=1, verbose=False):

    # Draw the cube
    lines = get_cube_lines(P, X, Y, Z, c)
    for line in lines:
        ax.add_line(line)

    # Print coordinates and size of cube if required
    if verbose:
        print "Drew a cube of size " + str(c) + " at " + str((X, Y, Z))


#########
# This function, with a closure for the animation object, executes the
# animation for the cube around X^2 + Y^2 = 7^2.
# Input arguments:
# fig and ax are the figure and axis used for drawing
# P is the perspective matrix
# c is the cube size
# path must be a parametrization from R to R^3, in our case
#   the circle path in polar coordinates
def show_cube_anim(fig, ax, P, nframes, c=1, path=circle_param, radius=7,
                   fps=50):

    # Calculates the current position around path
    X, Y, Z = path(0, nframes, radius)

    # Get lines for cube at current pos and add to axis
    lines = get_cube_lines(P, X, Y, Z, c)
    for line in lines:
        ax.add_line(line)
        line.set_visible(False)  # To prevent the first frame getting stuck

    # Returns the 'frames' for the cube animation. The cube should move
    # along a path given by path (which is a function R -> R^3)
    # Default radius 7 since 7^2 = 49
    def cube_animation(i, lines):
        # Calculates the current position around path
        X, Y, Z = path(i, nframes, radius)

        # Update lines for cube at current pos
        update_cube(lines, P, X, Y, Z, c, i)

        # Haven't found a better solution until now, see also:
        # http://stackoverflow.com/questions/21439489/matplotlib-animation-first-frame-remains-in-canvas-when-using-blit
        if i == 1:
            for line in lines:
                line.set_visible(True)

        return lines  # Return iterable lines list

    return animation.FuncAnimation(fig, cube_animation, frames=nframes,
                                   fargs=(lines,),
                                   interval=1000/fps, blit=True)


#########
# Shows the image plus the calibration points as Rein does.
def add_cal_points(xy, XYZ):
    plt.plot(xy[:, 0], xy[:, 1], 'bo')


##########
# image - img
# 2D calibration points - xy
# 3D calibration points - XYZ
# Whether to flip image when showing
#   (due to OS/jpg decoding issues) - flip_img
# Whether to show projection matrix - show_mat
# Whether to show calibration points overlayed on the image - show_cal
# Whether to show some cubes drawn onto the image - show_cubes
# Whether to show the cube animation across the image - show_anim
def main(imgpath, xy, XYZ, flip_img,
         show_mat, show_cal, show_cubes, show_anim, square_anim, save,
         nframes=100, cube_size=1, fps=50):

    # Find the calibration matrix P
    P = calibration_matrix(xy, XYZ)
    error = reproj_error(xy, XYZ, P)

    # Print P and its error if required
    if show_mat:
        print "Used the following matrix:"
        print P
        print "which has an error of " + str(error)

    # Plots
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    plt.axis('off')
    plt.axis('equal')

    # Plot the basic image
    img = plt.imread(imgpath)
    # On some machines the image is flipped upside down
    # when loaded, so an option is added to deal with this.
    if flip_img:
        img = np.flipud(img)
    plt.imshow(img)

    # Plot the calibration points if required
    if show_cal:
        add_cal_points(xy, XYZ)

    # Plot some cubes
    if show_cubes:
        draw_cube(ax, P, -5, 0, 1, 1)  # Cube on the right lower side
        draw_cube(ax, P, -5, 0, 5, 3)  # Cube on the right upper side
        draw_cube(ax, P, 0, -5, 0, 1)  # Cube on the left lower side

    if show_anim:
        # Removing assignment causes garbage collector to remove animation
        if square_anim:
            line_ani = show_cube_anim(fig, ax, P, nframes,
                                      path=square_param, c=cube_size, fps=fps)
        else:
            line_ani = show_cube_anim(fig, ax, P, nframes,
                                      c=cube_size, fps=fps)

        if save:
            line_ani.save('cube_animation.mp4', fps=fps)
    plt.show()


# Get arguments from command line.
def get_args():
    import argparse

    # Set up arguments and description
    parser = argparse.ArgumentParser(
            description="""Program that calibrates the image of a table
            with chessboard pattern given calibration points in 3D and 2D.
            Then cubes are (hard-coded) drawn and animated""",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--image', help='Image to use',
                        default='images/calibrationpoints.jpg', type=str)
    parser.add_argument('--cal2D', help='2D Calibration points',
                        default="data/2D_calibrationpoints", type=str)
    parser.add_argument('--cal3D', help='3D Calibration points',
                        default="data/3D_calibrationpoints", type=str)

    parser.add_argument('--nframes', help='Number of animation frames',
                        default=500, type=int)
    parser.add_argument('--cubesize', help='Size of the cube to animate',
                        default=1, type=float)
    parser.add_argument('--fps', help='Frames per second',
                        default=100, type=float)

    parser.add_argument('--flip', dest='flip', action='store_true',
                        help='Flip image')
    parser.add_argument('--no-flip', dest='flip', action='store_false')

    parser.add_argument('--showmat', dest='showmat', action='store_true',
                        help='Show calibration matrix')
    parser.add_argument('--no-showmat', dest='showmat', action='store_false')

    parser.add_argument('--showcal', dest='showcal', action='store_true',
                        help='Plot calibration points')
    parser.add_argument('--no-showcal', dest='showcal', action='store_false')

    parser.add_argument('--showcubes', dest='showcubes', action='store_true',
                        help='Plot example cubes onto image')
    parser.add_argument('--no-showcubes', dest='showcubes',
                        action='store_false')

    parser.add_argument('--showanim', dest='showanim', action='store_true',
                        help='Show animation of cube circling')
    parser.add_argument('--no-showanim', dest='showanim', action='store_false')

    parser.add_argument('--squareanim', dest='squareanim', action='store_true',
                        help='Show animation of cube around a square')
    parser.add_argument('--no-squareanim', dest='squareanim',
                        action='store_false')

    parser.add_argument('--saveanim', dest='saveanim', action='store_true',
                        help='Save animation of cube circling ')
    parser.add_argument('--no-saveanim', dest='saveanim', action='store_false')

    parser.set_defaults(flip=False, showmat=True, showcal=False,
                        showcubes=True, showanim=True, squareanim=False,
                        saveanim=False)
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    # Load the calibration points from text files
    xy = np.loadtxt(args.cal2D)
    XYZ = np.loadtxt(args.cal3D)

    main(args.image, xy, XYZ,
         args.flip, args.showmat, args.showcal,
         args.showcubes, args.showanim, args.squareanim, args.saveanim,
         nframes=args.nframes, cube_size=args.cubesize, fps=args.fps)
